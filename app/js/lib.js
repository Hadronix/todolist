function getList(){

    db.loadDatabase(function(){
        db.find({}).sort({time: 1}).exec(function(err, tasks){
            for(let key in tasks)
                create(tasks[key])
        })
    })

}

function create(task){

    let value
    let id
    
    if(!task){
         // Создаем новый
        value = $(this).val()
        time = Date.now()

        // Добавляем в базу
        db.insert({ value: value, time: time }, (err, savedTask) => {
            if (savedTask){
                draw(savedTask.value, savedTask._id)

                // Показываем всплывающее сообщение
                ipc.send('showPopup', value)
            }
        })

    // Восстанавливаем сохраненный таск
    } else {
        value = task.value
        id = task._id

        draw(value, id)
    }
}

function destroy(id){
    db.remove({ _id: id }, {}, (err, numRemoved) => {
        if (numRemoved){
            $(`[block-id="${id}"]`).fadeOut(500);
        }
    })
}

function draw(value, id){

    let template = `<div class="ui yellow segment animated fadeInLeft" block-id="${id}">
                        <div class="ui checkbox" id="${id}">
                            <input type="checkbox">
                            <label>${value}</label>
                        </div>
                    </div>`

     $(template).prependTo("#tasks-wrapper")
}
