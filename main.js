const {
    app,
    BrowserWindow,
    dialog,
    ipcMain,
    Menu,
    Tray
} = require('electron');

const path = require('path')
const url = require('url')


let mainWindow
let tray
const appMenu = Menu.buildFromTemplate([
    {
        label: 'Файл ',
        submenu: [{
            label: 'О программе',
            click: function (item, focusedWindow) {
                if (focusedWindow) {
                    const options = {
                        type: 'info',
                        title: 'О программе',
                        buttons: ['Ok'],
                        message: 'Todolist for MICROS'
                    }
                    // Вызываем дмаологовое окно
                    dialog.showMessageBox(focusedWindow, options)
                }
            }
        }, {
            label: 'Выйти из приложения',
            accelerator: 'CmdOrCtrl+W',
            role: 'quit'
        }]
    }
])

// Меню для трея
const trayMenu = Menu.buildFromTemplate(
    [
        {
            label: 'Показать', 
            type: 'normal', 
            click:  function(){
                mainWindow.show();
            } 
        },
        {   
            label: 'Выход', 
            type: 'normal', 
            role: 'quit'
        }
    ])

function appStart(){
    // Создаем меню
    Menu.setApplicationMenu(appMenu)

    // Создаем трей
    tray = new Tray(path.join(__dirname, 'icon.png'))
    tray.setContextMenu(trayMenu)

    // Создаем главное окно
    createWindow();

    ipcMain.on('showPopup', function(event, value){
        tray.displayBalloon({
            title: 'Добавлена новая заметка',
            content: value
            })
        }
    )
}


function createWindow() {

    mainWindow = new BrowserWindow({
        minWidth: 350,
        width: 350,
        height: 600,
        autoHideMenuBar: true,
        icon: path.join(__dirname, 'icon.png'),
    })


    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))


    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })

    mainWindow.on('minimize', function(event){
        event.preventDefault()
            mainWindow.hide();
    });

    //mainWindow.webContents.openDevTools()
}

// Приложение готово
app.on('ready', appStart)

// Если закрыты все окна, закрыть приложение
app.on('window-all-closed', function () {

    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
})